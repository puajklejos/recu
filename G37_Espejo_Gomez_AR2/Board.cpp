﻿#include "Board.h"

Board::Board()
{
	//STRINGS DE TAMAÑOS
	std::string BoardSizeString;
	std::string BlocksString;
	std::string RangePointsString;
	std::string NumColumnsS;
	std::string NumRowsS;
	std::string MaxRangePointsS;
	std::string MinRangePointsS;

	std::ifstream infile;
	infile.open("config.txt");

	//CONTADORES
	int countVars = 0;
	int countColumnsRows = 0;
	int countRangePoints = 0;
	Score = 0;
	int Shooted = 0;

	//SCORE
	int MaxRangePoints;
	int MinRangePoints;

	//COLOR
	color = GetStdHandle(STD_OUTPUT_HANDLE);

	//LEER TODO EL FICHERO
	if (infile.is_open())
	{
		while (!infile.eof())
		{
			if (countVars == 0)
			{
				std::getline(infile, BoardSizeString);
				countVars++;
			}
			else if (countVars == 1)
			{
				std::getline(infile, BlocksString);
				countVars++;
			}
			else if (countVars == 2)
			{
				std::getline(infile, RangePointsString);
			}
		}
	}
	infile.close();
	
	//SEPARAMOS FILAS Y COLUMNAS
	for (int i = 0; i < BoardSizeString.size(); i++) {
		if (countColumnsRows == 0)
		{
			if (BoardSizeString[i] == ';')
			{
				countColumnsRows++;
			}
			else
			{
				NumColumnsS += BoardSizeString[i];
			}
		}
		else if (countColumnsRows == 1)
		{
			NumRowsS += BoardSizeString[i];
		}
	}

	//SEPARAMOS MAX Y MIN DE RANDOMS
	for (int i = 0; i < RangePointsString.size(); i++) {
		if (countRangePoints == 0)
		{
			if (RangePointsString[i] == ';')
			{
				countRangePoints++;
			}
			else
			{
				MinRangePointsS += RangePointsString[i];
			}
		}
		else if (countRangePoints == 1)
		{
			MaxRangePointsS += RangePointsString[i];
		}
	}
	//ELIMINAOS EL ";" QUE NO NOS INTERESA
	BlocksString = RemoveChar(BlocksString, ';');
	NumRowsS = RemoveChar(NumRowsS, ';');
	MaxRangePointsS = RemoveChar(MaxRangePointsS, ';');

	//CONVERSION DE STRINGS A INTS
	Blocks = std::stoi(BlocksString);
	NumRows = std::stoi(NumRowsS);
	NumColumns = std::stoi(NumColumnsS);
	MinRangePoints = std::stoi(MinRangePointsS);
	MaxRangePoints = std::stoi(MaxRangePointsS);

	//CREAMOS TABLERO DINAMICO
	md = new char*[NumRows];
	for (int i = 0; i < NumRows; i++)
		md[i] = new char[NumColumns];

	//LLENAMOS QUEUE DE SCORE RANDOMS
	int NumberOfRandoms;
	NumberOfRandoms = (NumColumns - 2) * Blocks;
	for (int i = 0; i < NumberOfRandoms; i++)
	{
		//GENERAMOS RANDOMS
		ScoreQueue.push(rand() % MaxRangePoints + MinRangePoints);
	}

	//BOOLS
	Shoots = false;

	//AÑADIMOS TODOS LOS ELEMENTOS AL BOARD
	BoardComplete();
}

std::string Board::RemoveChar(std::string str, char c)
{
	std::string result;
	for (size_t i = 0; i < str.size(); i++)
	{
		char currentChar = str[i];
		if (currentChar != c)
			result += currentChar;
	}
	return result;
}

void Board::BoardComplete()
{
	//LIMITES
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			if (i == 0 || j == 0 || j == (NumColumns - 1) || i == (NumRows - 1))
			{
				md[i][j] = '+';
			}
			else
			{
				md[i][j] = ' ';
			}
		}
	}

	//BLOQUES
	for (int i = 1; i < (Blocks + 1); i++)
	{
		for (int j = 1; j < (NumColumns - 1); j++)
		{
			md[i][j] = '@';
		}
	}

	//VARS PARA LAS POSICIONES DEL JUGADOR
	PositionPlayer.x = (NumRows - 3);
	PositionPlayer.y = (NumColumns / 2);

	//PLAYER
	md[PositionPlayer.x][PositionPlayer.y] = '-';
	md[PositionPlayer.x][PositionPlayer.y - 1] = '^';
	md[PositionPlayer.x][PositionPlayer.y - 2] = '-';

}

void Board::BoardPrint()
{
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			if (md[i][j] == '+')
			{
				SetConsoleTextAttribute(color, 10);
			}
			else if (md[i][j] == '@')
			{
				SetConsoleTextAttribute(color, 1);
			}
			else if (md[i][j] == '*')
			{
				SetConsoleTextAttribute(color, FOREGROUND_RED);
			}
			std::cout << md[i][j];
		}
		std::cout << std::endl;
	}
}

void Board::BoardUpdate(bool key[])
{
	//LEFT
	if (key[0])
	{
		//VIGILAMOS LIMITES DE MAPA
		if ((PositionPlayer.y - 2) == 0)
		{
			md[PositionPlayer.x][0] = '^';
			md[PositionPlayer.x][NumColumns-1] = '-';
			md[PositionPlayer.x][2] = ' ';
		}
		if ((PositionPlayer.y - 1) == 0)
		{
			PositionPlayer.y = NumColumns+1;

			md[PositionPlayer.x][0] = '-';
			md[PositionPlayer.x][1] = ' ';
		}
		if ((PositionPlayer.y) == (NumColumns))
		{
			md[PositionPlayer.x][0] = '+';
			md[PositionPlayer.x][NumColumns-1] = '-';
		}
		//ACTUALIZAMOS POSICION DEL JUGADOR (LEFT)
		md[PositionPlayer.x][PositionPlayer.y - 1] = '-';
		md[PositionPlayer.x][PositionPlayer.y - 2] = '^';
		md[PositionPlayer.x][PositionPlayer.y - 3] = '-';

		//AGREGAMOS ESPACIOS A LA JUGADA ANTERIOR
		md[PositionPlayer.x][PositionPlayer.y] = ' ';

		//ACTUALIZAMOS VARIABLES DEL JUGADOR
		PositionPlayer.y = PositionPlayer.y - 1;

		//RESTABLECER BLOQUES
		if ((PositionPlayer.y) == (NumColumns - 2))
		{
			md[PositionPlayer.x][NumColumns - 1] = '+';
		}
	}
	//RIGHT
	else if (key[1])
	{
		//VIGILAMOS LIMITES DE MAPA
		if (PositionPlayer.y == NumColumns - 1)
		{
			md[PositionPlayer.x][0] = '-';
			md[PositionPlayer.x][NumColumns-1] = '^';
		}
		if (PositionPlayer.y == NumColumns)
		{
			PositionPlayer.y = 0;
			md[PositionPlayer.x][NumColumns - 1] = '-';
			md[PositionPlayer.x][NumColumns - 2] = ' ';
		}
		if (PositionPlayer.y == 1)
		{
			md[PositionPlayer.x][NumColumns - 1] = '+';
		}

		//ACTUALIZAMOS POSICION DEL JUGADOR (RIGHT)
		md[PositionPlayer.x][PositionPlayer.y + 1] = '-';
		md[PositionPlayer.x][PositionPlayer.y] = '^';
		md[PositionPlayer.x][PositionPlayer.y - 1] = '-';
		
		//AGREGAMOS ESPACIOS A LA JUGADA ANTERIOR
		md[PositionPlayer.x][PositionPlayer.y - 2] = ' ';

		//ACTUALIZAMOS VARIABLES DEL JUGADOR
		PositionPlayer.y = PositionPlayer.y + 1;

		//ARREGLOS VARIOS
		if (PositionPlayer.y == 3)
		{
			md[PositionPlayer.x][0] = '+';
		}
	}
	else if (key[2])
	{
		//POS INICIAL DE LA BALA
		if (Shooted == 0)
		{
			PositionBullet.x = PositionPlayer.x - 1;
			PositionBullet.y = PositionPlayer.y - 1;

			if ((PositionBullet.y) >= 1 && PositionBullet.y < (NumColumns - 1))
			{
				//ITERADOR
				std::vector<Vec2>::iterator it;
				it = Bullets.begin();

				//METEMOS LA VARIABLE EN EL VECTOR
				Bullets.insert(it, PositionBullet);

				if (!Bullets.empty())
				{
					Shoots = true;
				}
				Shooted++;
			}
		}
		else if (Shooted == 1)
		{
			Shooted = 0;
		}
		
	}

	if (Shoots == true)
	{
		for (int i = 0; i < Bullets.size(); i++)
		{
			//PRINTAR EN PANTALLA
			md[Bullets[i].x][Bullets[i].y] = '*';

			//UN SOLO DISPARO
			if (md[Bullets[i].x + 1][Bullets[i].y] != '^')
			{
					md[Bullets[i].x + 1][Bullets[i].y] = ' ';
			}

			//CHOQUE
			if (md[Bullets[i].x - 1][Bullets[i].y] == '@')
			{
				//ESPACIO EN EL LUGAR DEL BLOQUE
				md[Bullets[i].x - 1][Bullets[i].y] = ' ';

				//ESPACIO EN EL LUGAR DE LA BALA
				md[Bullets[i].x][Bullets[i].y] = ' ';

				//ELIMINAMOS BALA
				Bullets.erase(Bullets.begin() + i);

				//PUNTUACION
				Score += ScoreQueue.front();
				ScoreQueue.pop();
			}
			else if(md[Bullets[i].x - 1][Bullets[i].y] == '+')
			{
				//ESPACIO EN EL LUGAR DE LA BALA
				md[Bullets[i].x][Bullets[i].y] = ' ';

				//ELIMINAMOS BALA
				Bullets.erase(Bullets.begin() + i);
			}
			else
			{
				//ACTUALIZAMOS POSICION
				Bullets[i].x = Bullets[i].x--;
			}
		}
	}
}

void Board::BoardScore()
{
	std::cout << "Score: " << Score << std::endl;
}

Board::~Board()
{
	for (int i = 0; i < NumRows; i++)
		delete[]md[i];
	delete[]md;
	md = nullptr;
}
