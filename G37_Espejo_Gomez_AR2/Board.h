#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <ctime>
#include <Windows.h>
#include <queue>
#include <map>
#include <vector>
#include <conio.h>
#include <list>

//STRUCT VEC2
struct Vec2
{
	int x; 
	int y;
};

class Board
{
public:

	//ATRIBUTOS
	char **md;
	int NumColumns;
	int NumRows;

	//INTS
	int Blocks;
	int RangePoints;
	int Score;

	//PLAYER
	Vec2 PositionPlayer;

	//SCORE QUEUE
	std::queue<int> ScoreQueue;

	//DISPAROS
	Vec2 PositionBullet;
	std::vector<Vec2> Bullets;

	//COLOR
	HANDLE color;

	//SHOOTS
	bool Shoots;
	//NO ME HA SALIDO DISPAROS CONTINUOS... (ASI que dejo dispar en cuanto haya una separacion entre disparo)
	int Shooted;

	//CONSTRUCTOR
	Board();

	//FUNCTIONS
	std::string RemoveChar(std::string str, char c);
	void BoardComplete();
	void BoardPrint();
	void BoardUpdate(bool key[]);
	void BoardScore();

	//DESTRUCTOR
	~Board();


};
