#include "Board.h"

//CONTROL DE MENU
enum class GameState { MENU, PLAY, RANKING, EXIT, INPUT_RANKING};
//CONTROL INPUT
enum class InputKey { K_LEFT, K_RIGHT, K_SPACE, K_ESC, K_ENTER, K_NONE, K_NUMKEYS };

//STRUCT SORT
struct Sort
{
	std::string Name;
	int Score;
};

bool is_empty(std::ifstream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}

void main(int, char **)
{
	//RANDOM
	srand(time(NULL));


	//CREACION DE TABLERO
	Board b1;
	//SIZE
	int Size;
	int seventyfivePercent;
	int fiftyPercent;
	int twentyfivePercent;

	//INPUTS
	bool KeyBoard[(int)InputKey::K_NUMKEYS] = {};

	//CREACION DEL GAMESTATE
	GameState myGameState = GameState::MENU;
	//INPUTS GAMESTATE
	int MenuSelection;
	//COLOR
	HANDLE color = GetStdHandle(STD_OUTPUT_HANDLE);

	//NAME RANKINS
	std::string NameRanking;

	//MAP
	std::map<std::string, int> RankingMap;

	//RANKING
	int countNames = 0;
	std::string NamesWithScore1;
	std::string NamesWithScore2;
	std::string NamesWithScore3;
	std::string NamesWithScore4;
	std::string NamesWithScore5;
	std::string Name1;
	std::string Name2;
	std::string Name3;
	std::string Name4;
	std::string Name5;
	std::string Score1S;
	std::string Score2S;
	std::string Score3S;
	std::string Score4S;
	std::string Score5S;
	int Score1;
	int Score2;
	int Score3;
	int Score4;
	int Score5;
	int counterVars = 0;
	int counterNames = 1;
	std::ifstream infile;
	int number_of_lines = 0;
	std::string line;
	int i = 0;
	std::vector<Sort> v_Sort;
	int CounterNumRanking = 0;
	int counterSort = 0;
	int timing = 100;
	int Shooted = 0;
	int FirstSort = 0;

	//HOW MANY LINES
	std::ifstream myfile("ranking.txt");
	while (std::getline(myfile, line))
		++number_of_lines;

	//ARRAY DE STRINGS Y SCORE
	Sort SortToPrint[100];

	//BOOLS
	bool Update = false;
	bool InputRanking = false;
	bool Exit = false;
	bool Menu = false;
	bool ResetSize = false;

	//SCORE
	std::priority_queue<Sort> sort;
	Sort aux;

	//PRINT IN TXT
	std::ofstream HighScoreFile;

	infile.open("ranking.txt");
	if (!is_empty(infile))
	{

	if (infile.is_open())
	{
		while (!infile.eof())
		{
			if (number_of_lines == 1)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
			}
			if (number_of_lines == 2)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
			}
			if (number_of_lines == 3)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
				else if (countNames == 2)
				{
					std::getline(infile, NamesWithScore3);
					countNames++;
				}
			}
			if (number_of_lines == 4)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
				else if (countNames == 2)
				{
					std::getline(infile, NamesWithScore3);
					countNames++;
				}
				else if (countNames == 3)
				{
					std::getline(infile, NamesWithScore4);
					countNames++;
				}
			}
			if (number_of_lines == 5)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
				else if (countNames == 2)
				{
					std::getline(infile, NamesWithScore3);
					countNames++;
				}
				else if (countNames == 3)
				{
					std::getline(infile, NamesWithScore4);
					countNames++;
				}
				else if (countNames == 4)
				{
					std::getline(infile, NamesWithScore5);
					countNames++;
				}
			}
		}
	}
	//infile.close();

	std::ifstream in("ranking.txt");
		/*while (!in.eof())
		{
			if (number_of_lines == 1)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
			}
			if (number_of_lines == 2)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
			}
			if (number_of_lines == 3)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
				else if (countNames == 2)
				{
					std::getline(infile, NamesWithScore3);
					countNames++;
				}
			}
			if (number_of_lines == 4)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
				else if (countNames == 2)
				{
					std::getline(infile, NamesWithScore3);
					countNames++;
				}
				else if (countNames == 3)
				{
					std::getline(infile, NamesWithScore4);
					countNames++;
				}
			}
			if (number_of_lines == 5)
			{
				if (countNames == 0)
				{
					std::getline(infile, NamesWithScore1);
					countNames++;
				}
				else if (countNames == 1)
				{
					std::getline(infile, NamesWithScore2);
					countNames++;
				}
				else if (countNames == 2)
				{
					std::getline(infile, NamesWithScore3);
					countNames++;
				}
				else if (countNames == 3)
				{
					std::getline(infile, NamesWithScore4);
					countNames++;
				}
				else if (countNames == 4)
				{
					std::getline(infile, NamesWithScore5);
					countNames++;
				}
			}
		}*/
		//ARREGLO DE DATOS
		if (number_of_lines == 1)
		{
			for (int i = 0; i < NamesWithScore1.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore1[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name1 += NamesWithScore1[i];
					}
				}
				else if (counterVars == 1)
				{
					Score1S += NamesWithScore1[i];
				}
			}
			counterVars = 0;
		}
		if (number_of_lines == 2)
		{
			for (int i = 0; i < NamesWithScore1.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore1[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name1 += NamesWithScore1[i];
					}
				}
				else if (counterVars == 1)
				{
					Score1S += NamesWithScore1[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore2.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore2[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name2 += NamesWithScore2[i];
					}
				}
				else if (counterVars == 1)
				{
					Score2S += NamesWithScore2[i];
				}
			}
			counterVars = 0;
		}
		if (number_of_lines == 3)
		{
			for (int i = 0; i < NamesWithScore1.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore1[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name1 += NamesWithScore1[i];
					}
				}
				else if (counterVars == 1)
				{
					Score1S += NamesWithScore1[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore2.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore2[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name2 += NamesWithScore2[i];
					}
				}
				else if (counterVars == 1)
				{
					Score2S += NamesWithScore2[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore3.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore3[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name3 += NamesWithScore3[i];
					}
				}
				else if (counterVars == 1)
				{
					Score3S += NamesWithScore3[i];
				}
			}
			counterVars = 0;
		}
		if (number_of_lines == 4)
		{
			for (int i = 0; i < NamesWithScore1.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore1[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name1 += NamesWithScore1[i];
					}
				}
				else if (counterVars == 1)
				{
					Score1S += NamesWithScore1[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore2.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore2[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name2 += NamesWithScore2[i];
					}
				}
				else if (counterVars == 1)
				{
					Score2S += NamesWithScore2[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore3.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore3[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name3 += NamesWithScore3[i];
					}
				}
				else if (counterVars == 1)
				{
					Score3S += NamesWithScore3[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore4.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore4[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name4 += NamesWithScore4[i];
					}
				}
				else if (counterVars == 1)
				{
					Score4S += NamesWithScore4[i];
				}
			}
			counterVars = 0;
		}
		if (number_of_lines == 5)
		{
			for (int i = 0; i < NamesWithScore1.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore1[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name1 += NamesWithScore1[i];
					}
				}
				else if (counterVars == 1)
				{
					Score1S += NamesWithScore1[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore2.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore2[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name2 += NamesWithScore2[i];
					}
				}
				else if (counterVars == 1)
				{
					Score2S += NamesWithScore2[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore3.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore3[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name3 += NamesWithScore3[i];
					}
				}
				else if (counterVars == 1)
				{
					Score3S += NamesWithScore3[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore4.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore4[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name4 += NamesWithScore4[i];
					}
				}
				else if (counterVars == 1)
				{
					Score4S += NamesWithScore4[i];
				}
			}
			counterVars = 0;
			for (int i = 0; i < NamesWithScore5.size(); i++)
			{
				if (counterVars == 0)
				{
					if (NamesWithScore5[i] == ';')
					{
						counterVars++;
					}
					else
					{
						Name5 += NamesWithScore5[i];
					}
				}
				else if (counterVars == 1)
				{
					Score5S += NamesWithScore5[i];
				}
			}
			counterVars = 0;
		}

		//STRING TO INT(SCORE)
		if (number_of_lines == 1)
		{
			Score1 = std::stoi(Score1S);
			RankingMap[Name1] = Score1;
		}
		else if (number_of_lines == 2)
		{
			Score1 = std::stoi(Score1S);
			RankingMap[Name1] = Score1;
			Score2 = std::stoi(Score2S);
			RankingMap[Name2] = Score2;
		}
		else if (number_of_lines == 3)
		{
			Score1 = std::stoi(Score1S);
			RankingMap[Name1] = Score1;
			Score2 = std::stoi(Score2S);
			RankingMap[Name2] = Score2;
			Score3 = std::stoi(Score3S);
			RankingMap[Name3] = Score3;
		}
		else if (number_of_lines == 4)
		{
			Score1 = std::stoi(Score1S);
			RankingMap[Name1] = Score1;
			Score2 = std::stoi(Score2S);
			RankingMap[Name2] = Score2;
			Score3 = std::stoi(Score3S);
			RankingMap[Name3] = Score3;
			Score4 = std::stoi(Score4S);
			RankingMap[Name4] = Score4;
		}
		else if (number_of_lines == 5)
		{
			Score1 = std::stoi(Score1S);
			RankingMap[Name1] = Score1;
			Score2 = std::stoi(Score2S);
			RankingMap[Name2] = Score2;
			Score3 = std::stoi(Score3S);
			RankingMap[Name3] = Score3;
			Score4 = std::stoi(Score4S);
			RankingMap[Name4] = Score4;
			Score5 = std::stoi(Score5S);
			RankingMap[Name5] = Score5;
		}
	}
	infile.close();
	do
	{
		switch (myGameState)
		{
			case GameState::MENU:

				//MOSTRAMOS MENU
				system("cls");
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | BACKGROUND_BLUE);
				std::cout << "-*-*-*- MENU -*-*-*-" << std::endl;
				SetConsoleTextAttribute(color, 15);
				std::cout << "1- Play" << std::endl;
				std::cout << "2- Ranking" << std::endl;
				SetConsoleTextAttribute(color, 4);
				std::cout << "0- Exit" << std::endl;
				SetConsoleTextAttribute(color, 15);

				//SIZE
				if (ResetSize == false)
				{
					Size = b1.ScoreQueue.size();
					ResetSize = true;
				}

				//RECOGEMOS INPUT
				std::cin >> MenuSelection;

				//ELEGIMOS EL GAMESTATE SEGUN EL INPUT
				switch (MenuSelection)
				{
				case 0:
					myGameState = GameState::EXIT;
					break;
				case 1:
					myGameState = GameState::PLAY;
					break;
				case 2:
					myGameState = GameState::RANKING;
					break;
				default:
					system("cls");
					break;
				}

				break;
			case GameState::PLAY:
				do
				{
					system("cls");

					//MOSTRAMOS STRING DE PLAY
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | BACKGROUND_BLUE);
					std::cout << "-*-*-*- PLAY -*-*-*-" << std::endl;
					std::cout << std::endl;
					SetConsoleTextAttribute(color, 15);
					//MOSTRAMOS SCORE
					b1.BoardScore();

					//PRINTAMOS TABLERO
					b1.BoardPrint();

					//RECOGEMOS INPUTS
					KeyBoard[(int)InputKey::K_ESC] = GetAsyncKeyState(VK_ESCAPE);
					KeyBoard[(int)InputKey::K_LEFT] = GetAsyncKeyState(VK_LEFT);
					KeyBoard[(int)InputKey::K_RIGHT] = GetAsyncKeyState(VK_RIGHT);
					KeyBoard[(int)InputKey::K_SPACE] = GetAsyncKeyState(VK_SPACE);
					

					if (KeyBoard[(int)InputKey::K_ESC])
					{
						Exit = true;
					}

					//UPDATE GAME
					b1.BoardUpdate(KeyBoard);

					//CONTROL DE SLEEP
					//RANGOS
					seventyfivePercent = static_cast<int>(Size * 0.75);
					fiftyPercent = static_cast<int>(Size * 0.50);
					twentyfivePercent = static_cast<int>(Size * 0.25);

					if (b1.ScoreQueue.size() >= seventyfivePercent)
					{
						Sleep(timing);
					}
					else if (b1.ScoreQueue.size() <= seventyfivePercent && b1.ScoreQueue.size() >= fiftyPercent)
					{
						timing = 85;
						Sleep(timing);
					}
					else if (b1.ScoreQueue.size() <= fiftyPercent && b1.ScoreQueue.size() >= twentyfivePercent)
					{
						timing = 70;
						Sleep(timing);
					}
					else if (b1.ScoreQueue.size() <= twentyfivePercent)
					{
						timing = 60;
						Sleep(timing);
					}

					if (b1.ScoreQueue.empty())
					{
						InputRanking = true;
						Exit = true;
					}
					

				} while (!Exit);


				//FINALIZAMOS JUEGO
				if (Exit == true && InputRanking == false)
				{
					myGameState = GameState::EXIT;
				}
				else if (Exit == true && InputRanking == true)
				{
					myGameState = GameState::INPUT_RANKING;
				}

				break;
			case GameState::RANKING:

					system("cls");
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | BACKGROUND_BLUE);
					std::cout << "-*-*-*- RANKING -*-*-*-" << std::endl;
					SetConsoleTextAttribute(color, 10);

					//PRORITY QUEUE ORDENADA
					for (auto it = RankingMap.begin(); it != RankingMap.end(); it++)
					{
						aux.Name = it->first;
						aux.Score = it->second;
						sort.push(aux);

						if (CounterNumRanking<5)
						{
							CounterNumRanking++;
						}
					}

					//PRINTAR RANKING EN PANTALLA
					while (!sort.empty())
					{
						Sort aux1;
						aux1.Name = sort.top().Name;
						aux1.Score = sort.top().Score;

						//METEMOS EN UN AUX
						SortToPrint[counterSort] = aux1;
						counterSort++;
						sort.pop();
					}

					for (int j = 0; j < CounterNumRanking; j++)
					{
						std::cout << j+1 << "- " << SortToPrint[j].Name << " " << SortToPrint[j].Score << "\n";
					}

					//PRINTAR EN EL TXT
					HighScoreFile.open("ranking.txt");
					if (CounterNumRanking == 1)
					{
						HighScoreFile << SortToPrint[0].Name << ";" << SortToPrint[0].Score;
					}
					else if (CounterNumRanking == 2)
					{
						HighScoreFile << SortToPrint[0].Name << ";" << SortToPrint[0].Score << "\n";
						HighScoreFile << SortToPrint[1].Name << ";" << SortToPrint[1].Score;
					}
					else if (CounterNumRanking == 3)
					{
						HighScoreFile << SortToPrint[0].Name << ";" << SortToPrint[0].Score << "\n";
						HighScoreFile << SortToPrint[1].Name << ";" << SortToPrint[1].Score << "\n";
						HighScoreFile << SortToPrint[2].Name << ";" << SortToPrint[2].Score;
					}
					else if (CounterNumRanking == 4)
					{
						HighScoreFile << SortToPrint[0].Name << ";" << SortToPrint[0].Score << "\n";
						HighScoreFile << SortToPrint[1].Name << ";" << SortToPrint[1].Score << "\n";
						HighScoreFile << SortToPrint[2].Name << ";" << SortToPrint[2].Score << "\n";
						HighScoreFile << SortToPrint[3].Name << ";" << SortToPrint[3].Score;
					}
					else if (CounterNumRanking == 5)
					{
						HighScoreFile << SortToPrint[0].Name << ";" << SortToPrint[0].Score << "\n";
						HighScoreFile << SortToPrint[1].Name << ";" << SortToPrint[1].Score << "\n";
						HighScoreFile << SortToPrint[2].Name << ";" << SortToPrint[2].Score << "\n";
						HighScoreFile << SortToPrint[3].Name << ";" << SortToPrint[3].Score << "\n";
						HighScoreFile << SortToPrint[4].Name << ";" << SortToPrint[4].Score;
					}
					HighScoreFile.close();

					do
					{
						KeyBoard[(int)InputKey::K_ESC] = GetAsyncKeyState(VK_ESCAPE);
						if (KeyBoard[(int)InputKey::K_ESC])
						{
							Menu = true;
							myGameState = GameState::MENU;
						}
					} while (!Menu);

					//REINICIALIZAMOS NIVEL
					new (&b1) Board();

					//REINICIALIZAMOS VARIABLES
					CounterNumRanking = 0;
					counterSort = 0;
					Exit = false;
					InputRanking = false;
					Menu = false;
					ResetSize = false;
					timing = 100;
				break;
			case GameState::INPUT_RANKING:
				system("cls");

				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | BACKGROUND_BLUE);
				std::cout << "-*-*-*- GAME OVER -*-*-*-" << std::endl;
				std::cout << std::endl;
				SetConsoleTextAttribute(color, 10);
				std::cout << "Your score is " << b1.Score << std::endl;
				std::cout << "Please enter your name: " << std::endl;
				SetConsoleTextAttribute(color, 7);

				std::cin >> NameRanking;

				//GUARDAMOS EN MAP EL NUEVO SCORE
				RankingMap[NameRanking] = b1.Score;

				//PASAMOS A RANKING
				myGameState = GameState::RANKING;
				break;
			case GameState::EXIT:
				exit(0);
				break;
		}

	} while (myGameState != GameState::EXIT);
}

bool operator < (const Sort &a, const Sort &b)
{
	return a.Score < b.Score;
}